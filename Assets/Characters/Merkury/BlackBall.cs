﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackBall : MonoBehaviour
{
    public float speed;
    Rigidbody rb;
    public float DieTime;
    private float die;
    public float dmg;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>(); 
    }

    // Update is called once per frame
    void Update()
    {
        die += Time.deltaTime;
        rb.velocity = transform.forward * speed;
        if(die > DieTime)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        print("L");
        if (other.GetComponent<HPtest>() != null)
        {
            HPtest hp = GetComponent<HPtest>();
            hp.RecieveDamage(dmg);
            print("K");
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DownUI : MonoBehaviour
{
    public GameObject StatsMenu;
    bool stats = false;
    public void OpenStats()
    {
        print("stats");
        if (stats == false)
        {
            StatsMenu.SetActive(true);
            stats = true;
        }
        else
        {
            StatsMenu.SetActive(false);
            stats = false;
        }
    }
}

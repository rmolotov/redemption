﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSystem : MonoBehaviour
{ 

    [HideInInspector] public int level;
    [HideInInspector] public float experience;
    [HideInInspector] public float experienceRequired = 500;

     public CharController stats;
     public MerkurySpells ms;


    //На сколько мы умнажаем каждый элемент статистики
    [HideInInspector]public float UpdStCoaf = 1.1f;


    // Start is called before the first frame update
    void Start()
    {
        ms.GetComponent<MerkurySpells>();
        level = 1;
        experience = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Exp();

        /*if (Input.GetKeyDown(KeyCode.C) && level <=14)
        {
            RankUp();
            print(level);
        }*/
    }

    void RankUp()
    {
        EnemyAI ai = GetComponent<EnemyAI>();
        level += 1;
        experience = experience - experienceRequired;
        print("NewLevel");

        //Spells
        ms.UpgradeStats(UpdStCoaf);

        StatsUP();
        if (level % 6 == 0)
        {
            UpdStCoaf += 0.1f * level / 6;
        }
        ai.CastingSpell();
    }

    void Exp()
    {
        if(experience >= experienceRequired && level <=14)
        {
            RankUp();
        }
    }
    public void RecieveExp(float exp)
    {
        experience += exp;
    }

    void StatsUP()
    {
        EnemyAI ai = GetComponent<EnemyAI>();
        stats.BasicAttackDmg *= UpdStCoaf;
        experienceRequired *= UpdStCoaf;
        if (ai.maxHP <= ai.curHP)
        {
            ai.curHP = (ai.maxHP *= UpdStCoaf);
        }
        else
            ai.maxHP *= UpdStCoaf;

        if (ai.maxMana <= ai.mana)
        {
            ai.mana = (ai.maxMana *= UpdStCoaf);
        }
        else
            ai.maxMana *= UpdStCoaf;


    }
}

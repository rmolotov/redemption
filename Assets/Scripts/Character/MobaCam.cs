﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobaCam : MonoBehaviour
{

    public float scrollSpeed;

    public float TopBarrier;
    public float BotBarrier;
    public float RightBarrier;
    public float LeftBarrier;
    public GameObject player;

    private bool camLock = false;
    // Start is called before the first frame update
    void Start() { 
        //transform.position = new Vector3(GameManager.Instance().currentPlayer.transform.position.x, 15f, GameManager.Instance().currentPlayer.transform.position.z - 6);
        GameManager.Instance().cam = Camera.main;       
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (!GameManager.Instance().currentPlayer) return;

        if (Input.mousePosition.y >= Screen.height * TopBarrier)
            transform.Translate(Vector3.forward * Time.deltaTime * scrollSpeed, Space.World);

        if (Input.mousePosition.y <= Screen.height * BotBarrier)
            transform.Translate(Vector3.back * Time.deltaTime * scrollSpeed, Space.World);

        if (Input.mousePosition.x >= Screen.width * RightBarrier)
            transform.Translate(Vector3.right * Time.deltaTime * scrollSpeed, Space.World);

        if (Input.mousePosition.x <= Screen.width * LeftBarrier)
            transform.Translate(Vector3.left * Time.deltaTime * scrollSpeed, Space.World);

        if (Input.GetKeyDown(KeyCode.V))
            CamLock();
        if(camLock == true) {
            transform.position = new Vector3(GameManager.Instance().currentPlayer.transform.position.x, 15f, GameManager.Instance().currentPlayer.transform.position.z - 6);
        }
    }
    void CamLock()
    {
        if (camLock == false)
        {
            transform.position = new Vector3(GameManager.Instance().currentPlayer.transform.position.x, 15f, GameManager.Instance().currentPlayer.transform.position.z - 6);
            camLock = true;
        }
        else
            camLock = false;
    }
}


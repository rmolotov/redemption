﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;
using UnityEngine.UI;

public class CharController : NetworkBehaviour
{
    private Vector3 startPos;
    private float timer = 0f;
    private int _state;
    public int State
    {
        get { return _state; }
        set { _state = value; anim.SetInteger("state", _state); }
        
    }
    [Header("Components")]
    public Animator anim;
    public Movement mv;
    public NavMeshAgent agent;
    //Camera cam;
    //hp
    //hero

    [Header("Network")]
    [Range(1, 60)] public float LerpSpd;
    [SyncVar] Vector3 pos;
    [SyncVar] Quaternion rot;
    [SyncVar] int StateNet;

    //SPELLS
    //public float spell2_heal_count;
    public ParticleSystem BasAttEffect;



    //STATS
    [Header("Stats")]
    public float BasicAttackDmg;
    public float AttackRange;
    [HideInInspector] public float dmgBaff1 = 0;
    //[HideInInspector] public float dmgBaff2;

    //public float speed;

    //ATTACK
    [HideInInspector] public GameObject selectedUnit;
    EnemyAI enemyStats;


    //UI
    #region UI
    [Header("UI")]
    public Canvas UI;
    public Text Level;
    public Slider expSlider;
    public Slider HealthBar;
    public Image healthFill;
    public Text healthCount;
    public Slider manaSlider;
    public Text manaCount;
    LevelSystem level;
    EnemyAI ai;
    public Text killsUI;

    public Text dmg;
    public Text HP;
    public Text MN;
    public Text AR;
    #endregion


    //Spells
    [HideInInspector] public float SP1;
    [HideInInspector] public float SP2;
    [HideInInspector] public float SP3;


    //Stats
    [HideInInspector] public int kills = 0;
    public int ReturnUnitTime;
    public GameObject nightshade;
    [HideInInspector] public bool died = false;



    void Start()
    {
        startPos = transform.position;
        anim = GetComponent<Animator>();
        mv = GetComponent<Movement>();
        agent = GetComponent<NavMeshAgent>();
        level = GetComponent<LevelSystem>();
        ai = GetComponent<EnemyAI>();

        if (isLocalPlayer) GameManager.Instance().currentPlayer = this;
        else
        {
            agent.enabled = false;
        }
    }
    public void BasicAttack()
    {
        enemyStats.RecieveDamage(BasicAttackDmg + dmgBaff1, this.gameObject);
        if(selectedUnit != null)
            transform.LookAt(selectedUnit.transform);
        BasAttEffect.gameObject.SetActive(true);
        BasAttEffect.Play();
        dmgBaff1 = 0;
    }
    

    public void Move(Vector3 target)
    {
        State = 1;
        //selectedUnit = null;
        enemyStats = null;
        mv.MoveA(target);
    }
    public void Move(Vector3 target, bool resetTarget)
    {
        State = 1;
        enemyStats = null;
        mv.MoveA(target);
        if (resetTarget) selectedUnit = null;
    }
    public void Attack(EnemyAI enemy)
    {
        selectedUnit = enemy.gameObject;
        //selectedUnit = null;
        if (Vector3.Distance(transform.position, enemy.transform.position) <= AttackRange)
        {
            State = 2;
            selectedUnit = enemy.gameObject;
            enemyStats = enemy;
            transform.LookAt(selectedUnit.transform);
            agent.isStopped = true;
            BasAttEffect.transform.position = enemyStats.damgPoint.transform.position;
        }
        else
        {
            Move(enemy.transform.position);
        }
    }

    void Update()
    {
        if (died && timer < ReturnUnitTime)
        {
            timer += Time.deltaTime;
        }
        else if (died)
        {
            Respawn();
        }
        UIset();
        if (!isLocalPlayer)
        {
            transform.position = Vector3.Lerp(pos, transform.position, Time.deltaTime * LerpSpd);
            transform.rotation = Quaternion.Lerp(rot, transform.rotation, Time.deltaTime * LerpSpd);
            State = StateNet;
            return;
        }
        TransmitTransform();
        SpellCD();



        if (selectedUnit != null /*&& Vector3.Distance(selectedUnit.transform.position, transform.position) < AttackRange && State <= 2*/ && State <= 2)
        {
            EnemyAI e = selectedUnit.GetComponent<EnemyAI>();
            Attack(e);
        }
            


        //char logic:
        if (!selectedUnit && State == 2)
        {
            State = 0;
            agent.isStopped = false;
            
        }
        if (State == 2) return;
        if (agent.hasPath && State <= 2 && agent.isStopped == false)
            State = 1;
        else if(!agent.hasPath && State <=2)
            State = 0;
    }

    [Client]
    void TransmitTransform()
    {
        CmdSyncTransform(transform.position, transform.rotation, State);
    }

    [Command]
    void CmdSyncTransform(Vector3 p, Quaternion q, int state)
    {
        pos = p;
        rot = q;
        StateNet = state;
    }

    public void CastingSpell(int spellNumber)
    {
        MerkurySpells sp = GetComponent<MerkurySpells>();
        //State = 2 + spellNumber;
        sp.SpellCall(spellNumber);
        //agent.isStopped = true; 
    }

    void UIset()
    {
        //UI
        UI.transform.rotation = Quaternion.Euler(58, 0, 0);
        //Level
        Level.text = level.level.ToString();
        expSlider.maxValue = level.experienceRequired;
        expSlider.value = level.experience;


        //Health
        //HealthBar.fillAmount = health.curHP / health.curHP;
        HealthBar.maxValue = ai.maxHP;
        HealthBar.value = ai.curHP;
        healthCount.text = ai.curHP.ToString();

        //Mana
        manaSlider.maxValue = ai.maxMana;
        manaSlider.value = ai.mana;

        manaCount.text = (ai.mana.ToString());

        //Statistic
        killsUI.text = kills.ToString() + "/";
        dmg.text = "dmg " + BasicAttackDmg.ToString();
        HP.text = "HP " + ai.maxHP.ToString();
        MN.text = "Mana " + ai.maxMana.ToString();
        AR.text = "AttackRange " + AttackRange.ToString(); 
    }
    void SpellCD()
    {
        //SPELLS
        SP1 += Time.deltaTime;
        SP2 += Time.deltaTime;
        SP3 += Time.deltaTime;
    }
    public void Die()
    {
        print("KK");
        nightshade.SetActive(false);
        selectedUnit = null;
        agent.isStopped = true;
        UI.gameObject.SetActive(false);
        State = 0;
        CapsuleCollider cc = GetComponent<CapsuleCollider>();
        cc.enabled = false;
        died = true;
        timer = 0f;
    }
    public void Respawn()
    {
        if (isLocalPlayer) transform.position = startPos;
        nightshade.SetActive(true);
        UI.gameObject.SetActive(true);
        State = 0;
        CapsuleCollider cc = GetComponent<CapsuleCollider>();
        cc.enabled = true;
        died = false;
        ai.curHP = ai.maxHP;
    }
}
